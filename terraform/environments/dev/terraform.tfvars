#dev variables

environment = "dev"
aws_region = "eu-central-1"
ec2_instance_type = "t2.micro"
ec2_volume_type = "gp2"
ec2_volume_size = "8"
domain_name = "gygtesting.com"
db_username = "gygdb"
db_password = "plaintext" #only for testing purposes
db_name     = "db1"
