variable "environment" {
  description = "Current environment (dev/prod/etc)"
}

variable "aws_region" {
  description = "AWS provider region"
}

variable "ec2_instance_type" {
  description = "EC2 instance type"
}

variable "ec2_volume_type" {
  description = "EC2 instance volume type"
}

variable "ec2_volume_size" {
  description = "EC2 instance volume size"
}

variable "domain_name" {
  description = "The domain name for which the certificate should be issued"
  type        = string
}

variable "db_username" {
  description = "Database username"
  sensitive   = true
}

variable "db_password" {
  description = "Database password"
  sensitive   = true
}

variable "db_name" {
  description = "Name of Database"
}