# ec2 test with 'online' module
module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  name = "gyg-ec2-1"

  instance_type          = var.ec2_instance_type
  monitoring             = false

  subnet_id              = element(module.vpc.private_subnets, 0)

  root_block_device = [{
    volume_type = var.ec2_volume_type
    volume_size = var.ec2_volume_size
  }]

  tags = {
    Terraform   = "true"
    Environment = var.environment
  }

  # install nginx with the not-so-recommended terraform way
    user_data = <<-EOF
          #!/bin/bash
          sudo yum install -y nginx
          sudo systemctl start nginx
          sudo systemctl enable nginx
          EOF
  
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "gyg-vpc1"

  cidr = local.vpc_cidr

  azs                 = local.azs
  private_subnets     = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k)]
  public_subnets      = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 4)]
  database_subnets    = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 8)]

  private_subnet_names  = ["private1", "private2"]
  public_subnet_names   = ["public1", "public2"]
  database_subnet_names = ["data1", "data2"]

  create_database_subnet_group = true

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Environment = var.environment
  }
}

resource "aws_route53_zone" "private_zone" {
  name          = var.domain_name
  vpc {
    vpc_id     = module.vpc.vpc_id
  }
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 8.0"
  name = "my-alb"

  subnets = module.vpc.private_subnets
  vpc_id  = module.vpc.vpc_id

  security_groups = [module.vpc.default_security_group_id]


  tags = {
    Environment = var.environment
  }
}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 4.0"

  domain_name = var.domain_name
  zone_id     = aws_route53_zone.private_zone.zone_id
}

# Create A records for the ALB using the private domain name
resource "aws_route53_record" "alb_record" {
  zone_id = aws_route53_zone.private_zone.zone_id
  name    = "alb.gygtesting.com"  # Replace with your desired ALB record name
  type    = "A"

  alias {
    name    = module.alb.lb_dns_name
    zone_id = module.alb.lb_zone_id
    evaluate_target_health = false
  }
}

# Attach ACM certificate to the ALB
# resource "aws_lb_listener_certificate" "acm_cert" {
#   listener_arn    = module.alb.http_tcp_listeners[0]["listener_arn"]
#   certificate_arn = module.acm.acm_certificate_arn
# }



module "db" {
  source = "terraform-aws-modules/rds/aws"
  engine               = "postgres"
  engine_version       = "14"
  family               = "postgres14" # DB parameter group
  major_engine_version = "14"         # DB option group
  instance_class       = "db.t4g.large"
  allocated_storage     = 20
  max_allocated_storage = 100

  identifier            = "gyg-pg-db1"
  username              = var.db_username
  password              = var.db_password
  db_name               = var.db_name
  
  db_subnet_group_name   = module.vpc.database_subnet_group

  tags = {
    Name = "gyg-pg-db1"
    Environment = var.environment
  }
}
