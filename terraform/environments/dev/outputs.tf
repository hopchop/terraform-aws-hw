# outputs

output "ec2_properies_tags_all" {
  value = module.ec2-instance.tags_all
}

output "ec2_properies_instancetype" {
  value = module.ec2-instance.iam_instance_profile_arn
}

output "ec2_properties_privateIP" {
  value = module.ec2-instance.private_ip
}

output "ec2_properties_rootblockdev" {
  value = module.ec2-instance.root_block_device
}

output "db_instance_arn" {
  value = module.db.db_instance_arn
}

output "db_username" {
  value = module.db.db_instance_username
  sensitive = true
}

output "db_instance_name" {
  value = module.db.db_instance_name
}

output "db_endpoint" {
  value = module.db.db_instance_endpoint
}

output "db_engine" {
  value = module.db.db_instance_engine
}