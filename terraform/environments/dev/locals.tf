#local variables

locals {
  name   = "ex-${basename(path.cwd)}"
  region = "eu-central-1"

  vpc_cidr = "10.0.0.0/16"
  azs      = slice(data.aws_availability_zones.available.names, 0, 2)

}
