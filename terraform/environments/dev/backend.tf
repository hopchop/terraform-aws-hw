# state file - aws s3 bucket

terraform {
  backend "s3" {
    bucket = "gygrcn-test1"
    key    = "devtf.tfstate"
    region = "eu-central-1"
  }
}