import boto3
import json
import os

session = boto3.Session(profile_name="default", region_name="eu-central-1")

ec2_resource = session.resource(service_name="ec2")

instances_info = {}
instance_number = 1  # Initialize instance numbering

for instance in ec2_resource.instances.all():
    instance_info = {
        "ID": instance.id,
        "Type": instance.instance_type,
        "Public IPv4": instance.public_ip_address,
        "Private IPv4": instance.private_ip_address,
        "AMI": instance.image.id,
        "State": instance.state,
        "Volumes": []
    }

    for volume in instance.volumes.all():
        volume_info = {
            "VolumeId": volume.id,
            "VolumeType": volume.volume_type,
            "Size": volume.size,
            "State": volume.state
        }
        instance_info["Volumes"].append(volume_info)

    instances_info[f"instance{instance_number}"] = instance_info
    instance_number += 1  # Increment the instance number

# Convert the dictionary to JSON format
output_json = json.dumps(instances_info, indent=2)

# Test print the JSON output
print(output_json)

# Get the absolute path of the directory containing the script
script_directory = os.path.dirname(os.path.abspath(__file__))
file_path = os.path.join(script_directory, "instances.json")

# Write the JSON output to the JSON file (in the same directory as the script)
with open(file_path, "w") as json_file:
    json_file.write(output_json)